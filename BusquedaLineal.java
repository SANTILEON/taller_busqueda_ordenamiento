package binarialineal;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class BinariaLineal {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        ArrayList<Integer> listica = new ArrayList<Integer>();

        Random ran = new Random();

        int tamanio = 2000;
        int dato = 0;

        for (int i = 0; i < tamanio; i++) {
            dato = ran.nextInt(2000);
            listica.add(dato);
        }
        System.out.println(listica);
        System.out.println("tamano del arreglo"+listica.size());

        System.out.println("Ingrese numeroa buscar: ");
        int numBusqueda = sc.nextInt();

        
        long inicio = System.currentTimeMillis();
        //busqueda lineal
        for (int i = 0; i < listica.size(); i++) {
            if (numBusqueda == listica.get(i)) {
                System.out.println("El NUMERO: " + numBusqueda + " Se encuentra en la posicion: " + (i + 1));
                break;
            }
            if (numBusqueda > listica.size()) {
                System.out.println("el numero: " + numBusqueda + " No existe");
                break;
                
                
            }
        }

        long fin = System.currentTimeMillis();
                
                long tiempoTotal = fin - inicio;
                
        System.out.println("Tiempo total de ejecucion: " + tiempoTotal +" Milisegundos");

    }

}