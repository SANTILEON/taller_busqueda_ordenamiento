package binaria;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Binaria {
    
    static Scanner sc =  new Scanner(System.in);

     
    public static void main(String[] args) {
        
        Random ran = new Random();
        int tamano = 2000;
        int datoBuscar = 0;
        int [] arreglo = new int [tamano];
        
        
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = ran.nextInt(3000);
        }
        Arrays.sort(arreglo);
        
        for(int i = 0; i < arreglo.length; i++){
            System.out.println(arreglo[i] + " ");
        }
        
        
    
        System.out.println("Ingrese numero a buscar: ");
        datoBuscar = sc.nextInt();
        
        int inicio = 0;
        int fin = arreglo.length -1;
        int mitad = 0;
        int posicion =-1;
        
        long inicioo = System.currentTimeMillis();
        while(inicio <= fin){
            mitad = (inicio + fin)/2;
            if(arreglo[mitad] == datoBuscar){
                posicion = mitad+1;
                break;
            }else if(arreglo[mitad]<datoBuscar){
                inicio = mitad + 1;
                
            }else{
                fin = mitad -1;
        }
    }
        
    if(posicion !=-1){
         System.out.println("El dato " + datoBuscar + " fue encontrado en la posicion: " + posicion);
        
        
    }else{
       System.out.println("el dato "+ datoBuscar +" no fue encontrado en el arreglo");
        
    }
    
    long finn = System.currentTimeMillis();
                
                long tiempoTotal = finn - inicioo;
    
                 System.out.println("Tiempo total de ejecucion: " + tiempoTotal +" Milisegundos");
        
     
    }// cierra main
    
    
    
}//cierra class
